Inspired by the Pixel City by Shamus Young. This is the implementation of Pixel City using Ogre.

Refer to Shamus' blog for the Pixel City: http://www.shamusyoung.com/twentysidedtale/?p=2940

Pixel City source code: http://code.google.com/p/pixelcity/ 

License: GPL v2

Author: juho.yun https://code.google.com/u/juho.yun/